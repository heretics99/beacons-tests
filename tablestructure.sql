SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `beacons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `params` text,
  `remarks` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
