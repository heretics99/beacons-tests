<link href="bootstrap2/css/bootstrap.css" rel="stylesheet">
<link href="google-code-prettify/prettify.css" type="text/css" rel="stylesheet" />
<link href="bootstrap2/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/customcss.css" rel="stylesheet">

<?
include("config.php");
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

  $stmt = $db->prepare("SELECT id, date_created, params, remarks FROM beacons order by id desc limit 100");

  $stmt->execute();

  if($stmt->errorCode() == 0)
  {
    $results = $stmt->fetchAll();
  }
  else
  {
    $errors = $stmt->errorInfo();
    echo "SQL Error code- ".$errors[1].". Message-".$errors[2];
  }
  if(count($results)>0)
  {

?>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th>Date</th>
                <th>Params</th>
                <th>Remarks</th>
              </tr>
            </thead>
            <tbody>
            <?
              foreach($results as $k=>$row)
              {
              	$params = unserialize($row['params']);
              	$tmp = implode('<br>', array_map(function ($v, $k) { return '<font color="green">'.$k . '</font>=' . $v; }, $params, array_keys($params)));
	            echo "<tr>
                    <td>".$row['id']."</td>
                    <td>".$row['date_created']."</td>
                    <td>".$tmp."</td>
                    <td>".$row['remarks']."</td>
                  </tr>";
            
               }
            ?>
            </tbody>
          </table>

<?

  }
?>