<?
include("config.php");
$requests = serialize($_GET);
$copyOfRequest = $_GET;
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$stmt = $db->prepare("INSERT into beacons (params, remarks) VALUES(?,?)");
$stmt->bindParam(1,$requests);
$stmt->bindValue(2,"");

$stmt->execute();

if (isset($_REQUEST['redirect']))
{
	unset($copyOfRequest['redirect']);
	$redir = urldecode($_REQUEST['redirect']);
	if (preg_match("#https?://#", $redir) === 0) {
    	$redir = 'http://'.$redir;
	}
	// localhost/beacons-test/?q=world&redirect=google.com/search
	$redir .= "?".http_build_query($copyOfRequest);
	header( 'Location: '. ( $redir ) ) ;
}
header('Content-Type: image/png');
echo base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=');
?>